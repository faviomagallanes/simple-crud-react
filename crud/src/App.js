import { useState } from "react";
import { nanoid } from "nanoid";

const App = () => {
  const [tarea, setTarea] = useState("");
  const [tareas, setTareas] = useState([]);
  const [edicion, setEdicion] = useState(false);
  const [id, setId] = useState("");
  const [error, setError] = useState(null);

  const agregarTarea = (e) => {
    e.preventDefault();
    if (!tarea.trim()) {
      console.log("Elemento vacío");
      setError("Ingrese una tarea");
      return;
    }
    setTareas([{ id: nanoid(10), tarea }, ...tareas]);
    setTarea("");
    setError(null);
  };

  const eliminarTarea = (id) => {
    const arrayFiltrado = tareas.filter((item) => item.id !== id);
    setTareas(arrayFiltrado);
  };

  const editar = (item) => {
    setEdicion(true);
    setTarea(item.tarea);
    setId(item.id);
  };

  const editarTarea = (e) => {
    e.preventDefault();
    if (!tarea.trim()) {
      console.log("Elemento vacío");
      setError("Escriba algo por favor...");
      return;
    }

    const arrayEditado = tareas.map((item) =>
      item.id === id ? { id, tarea } : item
    );
    setTareas(arrayEditado);
    setEdicion(false);
    setTarea("");
    setId("");
    setError(null);
  };

  return (
    <div className="container">
      <h1 className="text-center mt-5">CRUD</h1>
      <hr />
      <div className="row">
        <div className="col-8">
          <h4 className="text-center">Tareas</h4>
          <ul className="list-group">
            {tareas.length === 0 ? (
              <li className="list-group-item">No hay Tareas</li>
            ) : (
              tareas.map((item) => (
                <li className="list-group-item" key={item.id}>
                  <span className="lead font-weight-bold">{item.tarea}</span>
                  <button
                    className="btn btn-danger btn-sm float-right mx-2"
                    onClick={() => eliminarTarea(item.id)}
                  >
                    Borrar
                  </button>
                  <button
                    className="btn btn-warning btn-sm float-right text-white"
                    onClick={() => editar(item)}
                  >
                    Editar
                  </button>
                </li>
              ))
            )}
          </ul>
        </div>
        <div className="col-4">
          <h4 className="text-center">
            {edicion ? "Editar Tarea" : "Agregar Tarea"}
          </h4>
          <form onSubmit={edicion ? editarTarea : agregarTarea}>
            {error ? <span className="text-danger">{error}</span> : null}
            <input
              type="text"
              className="form-control mb-2"
              placeholder="Ingresar tarea"
              onChange={(e) => setTarea(e.target.value)}
              value={tarea}
            />
            {edicion ? (
              <button
                className="btn btn-warning btn-block text-white"
                type="submit"
              >
                Editar
              </button>
            ) : (
              <button className="btn btn-success btn-block" type="submit">
                Ingresar
              </button>
            )}
          </form>
        </div>
      </div>
    </div>
  );
};

export default App;
